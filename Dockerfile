FROM node:alpine
WORKDIR /app
COPY . /app
RUN npm install
CMD node index.js
ENV PORT=8080
EXPOSE 8080
