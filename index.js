const express = require('express');
const app = express();

const PORT = process.env.PORT;

app.get('/', (req, res) => {
  res.send('Hello world from docker. This is a starting app');
})

app.listen(PORT, () => {
  console.log(`Running on port ${PORT}`);
})
